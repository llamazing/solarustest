local item = ...

function item:on_started()
    self:set_savegame_variable('possession_' .. item:get_name())
end