local item = ...
-- ripped from Tunics.solarus

local game = item:get_game()

function item:on_created()

    self:set_savegame_variable('possession_' .. item:get_name())
    self:set_assignable()
end

function item:on_obtained()

    -- zentropy.game.assign_item(self)
    if game:get_item_assigned(1) == nil then
      game:set_item_assigned(1, item)
    elseif game:get_item_assigned(2) == nil then
      game:set_item_assigned(2, item)
    end
end

function item:on_using()

    game:get_hero():start_hookshot()
end
