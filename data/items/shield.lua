local item = ...
--ripped from Tunics.solarus

function item:on_created()

    self:set_savegame_variable('possession_' .. item:get_name())
end

function item:on_variant_changed(variant)
  -- The possession state of the shield determines the built-in ability "shield".
  self:get_game():set_ability("shield", variant)
end

