-- This script initializes game values for a new savegame file.
-- You should modify the initialize_new_savegame() function below
-- to set values like the initial life and equipment
-- as well as the starting location.
--
-- Usage:
-- local initial_game = require("scripts/initial_game")
-- initial_game:initialize_new_savegame(game)

local initial_game = {}

-- rando needs to be global for the load_string method to function properly...
rando = require("scripts/rando")

-- Sets initial values to a new savegame file.
function initial_game:initialize_new_savegame(game)

  -- You can modify this function to set the initial life and equipment
  -- and the starting location.
  game:set_starting_location("overworld", "start_game")  -- Starting location.

  game:set_max_life(12)
  game:set_life(game:get_max_life())
  game:set_max_money(100)
  --game:set_ability("lift", 1)
  --game:set_ability("sword", 1)

  -- RANDOMIZER STUFF STARTS HERE

  -- The seed will be taken from player input in the future, but right now this will suffice
  local seed_text = sol.main.seed_text
  if seed_text == nil then
    seed_text = "12"
  end

  -- let the fun begin ;)
  rando:generate(game, seed_text)

  -- set a savegame_variable to a chest name and the item you want to give to the player to test dynamic loot from chests
  -- CAUTION! Doing this after generating a dynamic layout might brake the logic and invalidate the run!
  -- game:set_value("loot_start_chest_2", "heart_container")
  -- game:set_value("loot_start_pickable_1", "bombs_counter")
  
 
end

return initial_game
