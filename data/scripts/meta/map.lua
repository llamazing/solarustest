local map_meta = sol.main.get_metatable("map")

function map_meta:update_pickables()
  local game = self:get_game()
  for picky in self:get_entities_by_type("pickable") do
    local name = picky:get_name()
    local sgv = game:get_value("loot_" .. name)
    local sgvv = game:get_value("loot_variant_" .. name)
    if sgv ~= nil then
      local x, y, l = picky:get_position()
      local variant = 1
      if sgvv ~= nil then
        variant = sgvv
      end
      picky:remove()
      self:create_pickable({
        name = name,
        layer = l,
        x = x,
        y = y,
        treasure_name = sgv,
        treasure_variant = variant,
        treasure_savegame_variable = name,
      })
    end
  end
end