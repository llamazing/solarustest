local chest_meta = sol.main.get_metatable("chest")

-- override the builtin on_opened() method to enable randomly distributed loot
function chest_meta:on_opened(treasure_item, treasure_variant, treasure_savegame_variable)
  if treasure_savegame_variable ~= nil then
    local game = self:get_game()
    local hero = game:get_hero()
    local location, _ = treasure_savegame_variable:gsub("^opened_", "")
    print("opened chest " .. location)
    local loot = game:get_value("loot_" .. location)
    if loot ~= nil then
      print("awarding dynamic loot: " .. loot)
      hero:start_treasure(loot)
    else
      print("awarding static loot: " .. treasure_item:get_name())
      hero:start_treasure(treasure_item:get_name(), treasure_variant)
    end
  else
    self:get_game():get_hero():unfreeze()
  end
end