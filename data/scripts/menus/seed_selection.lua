local seed_selection_menu = {
  seed_names = {
    "12", "13", "14", "test", "demo",
  },
  config = {
    card_width = 96,
    card_height = 24,
    spacing = 8,
    seed_margin = 4,
    num_rows = 4,
    num_columns = 2,
    cursor_x = 0,
    cursor_y = 0,
    cursor_row = 0,
    cursor_column = 0,
    cursor_offsets = { {-5, 8}, {48, 21}, {93, 8}, {48, -5} },
  }
}

function seed_selection_menu:on_started()
  print("Initializing seed_selection_menu...")

  local quest_width, quest_height = sol.video.get_quest_size()
  self.config.quest_width = quest_width
  self.config.quest_height = quest_height
  self.config.grid_width = self.config.num_columns * (self.config.card_width + self.config.spacing) - self.config.spacing
  self.config.grid_height = self.config.num_rows * (self.config.card_height + self.config.spacing) - self.config.spacing
  self.config.grid_x = quest_width / 2 - self.config.grid_width / 2
  self.config.grid_y = quest_height / 2 - self.config.grid_height / 2
  self.config.cursor_sprite = sol.sprite.create("menus/arrow")

  self:update_cursor()
  self:build_background()
  -- self.config.grid_surface:clear()
end


function seed_selection_menu:on_draw(dst_surface)
  self.config.grid_surface:draw(dst_surface, self.config.grid_x, self.config.grid_y)
  for index, offset in ipairs(self.config.cursor_offsets) do
    self.config.cursor_sprite:set_direction(index - 1)
    self.config.cursor_sprite:draw(dst_surface, self.config.cursor_x + offset[1], self.config.cursor_y + offset[2])
  end
end

function seed_selection_menu:on_key_pressed(command, modifier)
  -- input handling
  print("Key pressed: " .. command)
  local handled = false
  if command == "right" then
    sol.audio.play_sound("cursor")
    self.config.cursor_column = (self.config.cursor_column + 1) % self.config.num_columns
    self:update_cursor()
    handled = true
  elseif command == "up" then
    sol.audio.play_sound("cursor")
    self.config.cursor_row = (self.config.cursor_row - 1) % self.config.num_rows
    self:update_cursor()
    handled = true
  elseif command == "left" then
    sol.audio.play_sound("cursor")
    self.config.cursor_column = (self.config.cursor_column - 1) % self.config.num_columns
    self:update_cursor()
    handled = true
  elseif command == "down" then
    sol.audio.play_sound("cursor")
    self.config.cursor_row = (self.config.cursor_row + 1) % self.config.num_rows
    self:update_cursor()
    handled = true
  elseif command == "c" or command == "space" then
    sol.main.seed_text = self.config.seed_name
    sol.audio.play_sound("open_lock")
    sol.menu.stop(self)
  end
end

function seed_selection_menu:update_cursor()
  self.config.cursor_x = self.config.grid_x + (self.config.cursor_column * (self.config.card_width + self.config.spacing))
  self.config.cursor_y = self.config.grid_y + (self.config.cursor_row * (self.config.card_height + self.config.spacing))
  local index = self.config.cursor_row * self.config.num_columns + self.config.cursor_column + 1
  self.config.seed_name = self.seed_names[index]
end

function seed_selection_menu:build_background()
  local grid_surface = sol.surface.create(self.config.grid_width, self.config.grid_height)

  local card_img = sol.surface.create(self.config.card_width, self.config.card_height)
  card_img:fill_color({140, 140, 140})
  card_img:set_opacity(150)  

  local seed_surface = sol.text_surface.create(
    {
      horizontal_alignment = "left",
      vertical_alignment = "bottom",
      font = "8_bit",
    }
  )

  local dst_x, dst_y = 0, 0

  for row = 1, self.config.num_rows do
    for column = 1, self.config.num_columns do
      card_img:draw(grid_surface, dst_x, dst_y)
      local idx = (row - 1) * self.config.num_columns + column
      if idx <= #self.seed_names then
        seed_surface:set_text(self.seed_names[idx])
        seed_surface:draw(grid_surface, dst_x, dst_y + self.config.card_height - self.config.seed_margin)
      end
      dst_x = dst_x + self.config.card_width + self.config.spacing
    end
    dst_y = dst_y + self.config.card_height + self.config.spacing
    dst_x = 0
  end
  self.config.grid_surface = grid_surface
end

return seed_selection_menu