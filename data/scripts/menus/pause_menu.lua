local pause_menu_builder = {}

local language_manager = require("scripts/language_manager")

local item_names = {
  "bow",
  "hookshot",
  "bombs_counter",
}

local square_size = 24
local spacing = 8
local item_margin = 4
local num_rows, num_columns = 4, 5
local grid_width = num_columns * (square_size + spacing) - spacing
local grid_height = num_rows * (square_size + spacing) - spacing
local quest_width, quest_height = sol.video.get_quest_size()
local grid_x = quest_width / 2 - grid_width / 2
local grid_y = quest_height / 2 - grid_height / 2

local square_img = sol.surface.create(square_size, square_size)
square_img:fill_color({40, 40, 40})
square_img:set_opacity(150)

local item_sprite = sol.sprite.create("entities/items")

local function build_grid_background(grid_surface)
  
  local dst_x, dst_y = 0, 0

  for row = 1, num_rows do
    for column = 1, num_columns do
      square_img:draw(grid_surface, dst_x, dst_y)
      dst_x = dst_x + square_size + spacing
    end
    dst_y = dst_y + square_size + spacing
    dst_x = 0
  end
end

local function build_items(grid_surface, game)
  local dst_x, dst_y = 0, 0
  local amount_text = sol.text_surface.create({
    horizontal_alignment = "right",
    vertical_alignment = "bottom",
    font = "8_bit",
  })
  for index, item_name in ipairs(item_names) do
    local item = game:get_item(item_name)
    local variant = item:get_variant()
    if variant > 0 then
      item_sprite:set_animation(item_name)
      item_sprite:set_direction(variant - 1)
      local origin_x, origin_y = item_sprite:get_origin()
      item_sprite:draw(grid_surface, dst_x + origin_x + item_margin, dst_y + origin_y + item_margin)
      if item:has_amount() then
        amount_text:set_text(item:get_amount())
        amount_text:draw(grid_surface, dst_x + square_size, dst_y + square_size)
      end
    end
    if index % num_columns == 0 then
      dst_y = dst_y + square_size + spacing
      dst_x = 0
    else
      dst_x = dst_x + square_size + spacing
    end
  end
end

function pause_menu_builder:create(game)
  local pause_menu = {}
  local grid_surface = sol.surface.create(grid_width, grid_height)
  local cursor_sprite = sol.sprite.create("menus/arrow")
  local cursor_x, cursor_y = 0, 0
  local cursor_row, cursor_column = 0, 0
  local cursor_offsets = {
    {-5, 8},
    {9, 21},
    {21, 8},
    {9, -5}
  }
  local font, font_size = language_manager:get_menu_font()
  local item_name_text = sol.text_surface.create({
    horizontal_alignment = "center",
    vertical_alignment = "top",
    font = font,
    font_size = font_size,
  })

  local function update_cursor()
    cursor_x = grid_x + (cursor_column * (square_size + spacing))
    cursor_y = grid_y + (cursor_row * (square_size + spacing))
    local index = cursor_row * num_columns + cursor_column + 1
    local item_name = item_names[index]
    if item_name ~= nil and game:has_item(item_name) then
      item_name_text:set_text_key("inventory.items." .. item_name)
    else
      item_name_text:set_text(nil)
    end
  end

  local function assign_item(slot)
    local index = cursor_row * num_columns + cursor_column + 1
    local item_name = item_names[index]
    if item_name == nil then
      return
    end

    local item = game:get_item(item_name)
    local variant = item:get_variant()
    if variant == 0 then
      return
    end

    sol.audio.play_sound("picked_item")
    local other_slot = 3 - slot
    local other_item = game:get_item_assigned(other_slot)
    if other_item == item then
      game:set_item_assigned(other_slot, game:get_item_assigned(slot))
    end
    game:set_item_assigned(slot, item)
  end

  function pause_menu:on_started()
    grid_surface:clear()
    build_grid_background(grid_surface)
    build_items(grid_surface, game)
    update_cursor()
  end

  function pause_menu:on_draw(dst_surface)
    grid_surface:draw(dst_surface, grid_x, grid_y)
    for index, offset in ipairs(cursor_offsets) do
      cursor_sprite:set_direction(index - 1)
      cursor_sprite:draw(dst_surface, cursor_x + offset[1], cursor_y + offset[2])
    end
    item_name_text:draw(dst_surface, quest_width / 2, grid_y + grid_height + spacing)
  end

  function pause_menu:on_command_pressed(command)
    local handled = false
    if command == "right" then
      sol.audio.play_sound("cursor")
      cursor_column = (cursor_column + 1) % num_columns
      update_cursor()
      handled = true
    elseif command == "up" then
      sol.audio.play_sound("cursor")
      cursor_row = (cursor_row - 1) % num_rows
      update_cursor()
      handled = true
    elseif command == "left" then
      sol.audio.play_sound("cursor")
      cursor_column = (cursor_column - 1) % num_columns
      update_cursor()
      handled = true
    elseif command == "down" then
      sol.audio.play_sound("cursor")
      cursor_row = (cursor_row + 1) % num_rows
      update_cursor()
      handled = true
    elseif command == "item_1" then
      assign_item(1)
      handled = true
    elseif command == "item_2" then
      assign_item(2)
      handled = true
    end
    return handled
  end

  return pause_menu
end

return pause_menu_builder