-- Randomization magic is defined here
local rando = {
  -- locationss keep track of the accessibility logic for and the randomly chosen loot to acquire at a location
  locations = {
    start_chest_1 = {
      logic = "true",
      loot = ""
    },
    start_chest_2 = {
      logic = "true",
      loot = ""
    },
    start_chest_3 = {
      logic = "true",
      loot = ""
    },
    start_chest_4 = {
      logic = "rando:has('glove')",
      loot = ""
    },
    start_chest_5 = {
      logic = "rando:has('flippers') or rando:has('hookshot')",
      loot = ""
    },
    start_chest_6 = {
      logic = "rando:sgvar('start_variant') == 1 or (rando:sgvar('start_variant') == 2 and rando:has('glove')) or (rando:sgvar('start_variant') == 3 and (rando:has('hookshot') or rando:has('flippers')))",
      loot = ""
    },
    start_chest_7 = {
      logic = "rando:has('bow') and (rando:sgvar('start_variant') == 1 or (rando:sgvar('start_variant') == 2 or rando:has('glove')) or (rando:sgvar('start_variant') == 3 and rando:has('hookshot')))",
      loot = ""
    },
    start_chest_8 = {
      logic = "rando:has('bombs_counter')",
      loot = ""
    },
    start_pickable_1 = {
      logic = "true",
      loot = "",
    },
  },
  
  -- a temporary inventory just for the randomization process
  inventory = {},

  -- a list of all items that can be randomized and that are important for progression
  important_items = {"bow", "bombs_counter", "flippers", "glove", "hookshot", "sword", },

  -- a list of minor items, not strictly necessary to reach any location and beating the game
  filler_items = {"heart_container", "tunic", "heart_container", },

  dungeon_definitions = {
    {
      id = "start_variant",
      max_variant = 3,
      default_variant = 1,
    },
  }
}

-- Get a save game value, one of the more frequently used logic functions
function rando:sgvar(variable)
  return self.game:get_value(variable)
end

-- shuffle dungeon layouts
function rando:shuffle_dungeons()
  for _, dungeon in ipairs(self.dungeon_definitions) do
    local variant = math.random(1, dungeon.max_variant)
    self.game:set_value(dungeon.id, variant)
  end
end

-- reset dungeons to their default route
function rando:reset_dungeons()
  for _, dungeon in ipairs(self.dungeon_definitions) do
    self.game:set_value(dungeon.id, dungeon.default_variant)
  end
end

-- Check the current inventory during a randomization attempt, most used logic function!
function rando:has(item)
  for key, val in pairs(self.inventory) do
    if val == item then
      return true
    end
  end
  return false
end

-- only used internally during a randomization attempt
function rando:inventory_add_item(item, value)
  self["inventory"][item] = value
end

-- probably not needed at all, just for completeness sake
function rando:inventory_remove_item(item)
  self["inventory"][item] = nil
end

-- used to start a fresh attempt at generating a randomized loot distribution
function rando:inventory_clear()
  while #self.inventory > 0 do
    table.remove(self.inventory)
  end
end

-- used to start a fresh attempt at generating a randomized loot distribution
function rando:locations_clear()
  for location, _ in pairs(self["locations"]) do
    self["locations"][location]["loot"] = ""
  end
end

-- set the loot for a loot location
function rando:chests_add_loot(location, item)
  self["locations"][location]["loot"] = item
end

-- checks if a location is accessible with the items currently in the inventory (at the randomization phase, not while actively playing the game!)
function rando:is_accessible(location)
  local f = loadstring("return " .. self["locations"][location]["logic"])
  return f()
end

-- does this location already have loot assigned to itself?
function rando:is_assigned(location)
  return self["locations"][location]["loot"] ~= ""
end

-- save the finished loot distribution to save game variables prefixed by "loot_"
function rando:save()
  for location, data in pairs(self["locations"]) do
    self.game:set_value("loot_" .. location, data["loot"])
  end
end

-- for debug purposes only, output if a certain location is accessible at some point during randomization
function rando:test(location)
  print(location .. ": " .. self:is_accessible(location))
end

-- translate the seed_text string to a numeric value to seed the RNG
function rando:get_value_of_seed_text(seed_text)
  local available_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_.,;:!? "
  local seed_value = 0
  local input_length = #seed_text
  local available_length = #available_chars
  -- print(input_length .. " " .. available_length)
  for i = 1, input_length do
    local pos = available_chars:find(seed_text:sub(i, i))
    -- print(i .. " " .. seed_text:sub(i, i) .. " " .. pos .. " " .. seed_value)
    -- ignore characters in the seed_text that are not in the list of available characters
    if pos ~= nil then
      seed_value = seed_value + ( (pos - 1) * (available_length ^ (i - 1)) )
    end
  end
  print("Seed ("  .. seed_text .. "): " .. seed_value)
  return seed_value
end

function rando:init_rng(seed_text)
  local seed_value = self:get_value_of_seed_text(seed_text)
  self.game:set_value("seed_value", seed_value)
  math.randomseed(seed_value)
end

-- generate a randomized item distribution that allows the player to reach all loot locations
function rando:generate(game, seed_text)
  local temp_important_items, temp_filler_items, accessible
  local loops = 10

  self["game"] = game
  game:set_value("seed_text", seed_text)
  if seed_text == nil or seed_text == "" then
    -- seems like we are playing the game as envisioned in the editor
    -- but we have to make sure to set the variant of each dungeon to its default layout
    self:reset_dungeons()
    return
  end

  -- first and foremost: set the random number generator to the requested value
  self:init_rng(seed_text)

  -- this needs to be done before randomizing the loot, as the loot locations' logic
  -- might depend on the variant of the dungeon it is in
  self:shuffle_dungeons()

  repeat
    loops = loops - 1
    self:locations_clear()
    self:inventory_clear()
    temp_important_items = {}
    for _, imp in pairs(self["important_items"]) do
      temp_important_items[#temp_important_items + 1] = imp
    end
    temp_filler_items = {}
    for _, fil in pairs(self["filler_items"]) do
      temp_filler_items[#temp_filler_items + 1] = fil
    end
    print("Loop: " .. loops .. ", #TII: " .. #temp_important_items .. ", #TFI: " .. #temp_filler_items)
    -- self.inventory[#self.inventory + 1] = "bow"
    repeat
      accessible = {}
      for loc, data in pairs(self["locations"]) do
        if self:is_accessible(loc) and not self:is_assigned(loc) then
          accessible[#accessible + 1] = loc
        end
      end
      print("There are " .. #accessible .. " loot locations with the current inventory")
      if #accessible > 0 then
        local random_location_index = math.random(1, #accessible)
        local random_location = table.remove(accessible, random_location_index)
        local random_item, random_item_index
        if #temp_important_items > 0 then
          random_index = math.random(1, #temp_important_items)
          random_item = table.remove(temp_important_items, random_index)
          print("Selecting " .. random_location .. " to award " .. random_item)
          self.locations[random_location].loot = random_item
          table.insert(self.inventory, random_item)
        elseif #temp_filler_items > 0 then
          random_index = math.random(1, #temp_filler_items)
          random_item = table.remove(temp_filler_items, random_index)
          print("Selecting " .. random_location .. " to award " .. random_item)
          self.locations[random_location].loot = random_item
          table.insert(self.inventory, random_item)
        end
      end
    until #accessible == 0
    print(#temp_important_items .. ", " .. #temp_filler_items)
    --temp_important_items = {}
    --temp_filler_items = {}
  until loops < 0 or (#temp_important_items == 0 and #temp_filler_items == 0)
  self:save()
end

return rando