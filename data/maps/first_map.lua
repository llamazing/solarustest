-- Lua script of map first_map.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()

  -- You can initialize the movement and sprites of various
  -- map entities here.

  -- the next line must appear in any map that has randomized pickables (items visibly on the ground)
  map:update_pickables()

  -- Each dungeon gets a randomized variant, it can do whatever it wants with it.
  -- Here, we are moving a room connector (a.k.a. door) to branch pathing
  local start_variant = game:get_value("start_variant")
  local dx = { 0, -64, 64 }

  print("Map first_map, variant " .. start_variant)

  for variant_tile in map:get_entities("start_variant_") do
    local x, y, l = variant_tile:get_position()
    variant_tile:set_position(x + dx[start_variant], y, l)
  end

end

function start_bow_switch:on_activated()
  map:open_doors("start_door_final_room_")
end

function start_sensor_entrance:on_activated()
  hero:freeze()
  hero:set_animation("walking")
  local movement = sol.movement.create("straight")
  movement:set_angle(math.pi / 2)
  movement:set_speed(hero:get_walking_speed())
  movement:set_max_distance(32)
  movement:start(hero, function()
    hero:set_animation("stopped")
    sol.timer.start(map, 500, function()
      game:start_dialog("start.entering_dungeon", function()
        hero:unfreeze()
        start_sensor_entrance:remove()
      end)
    end)
  end)
end