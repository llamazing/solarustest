-- Lua script of map overworld.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map is loaded.
function map:on_started()

  -- You can initialize the movement and sprites of various
  -- map entities here.

  local start_variant = game:get_value("start_variant")
  local dx = { 0, -40, 40 }
  local dy = { 0, 8, 8 }

  print("Map overworld, start_variant " .. start_variant)

  for variant_tile in map:get_entities("start_variant_") do
    local x, y, l = variant_tile:get_position()
    variant_tile:set_position(x + dx[start_variant], y + dy[start_variant], l)
  end
end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

end
