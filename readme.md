# Solarus Test Project

This is a test project using the [Solarus engine](https://github.com/christopho/solarus) to create a 2D Zelda like game with a built-in randomizer. It will be able to shuffle both loot from chests as well as dungeon layouts. We may also add boss shuffle later on.

This means, to play this game, you need to to download the Solarus Launcher from the [Solarus website](https://www.solarus-games.org/download/). The page also includes more instructions about adding the game to the launcher and running it.

As this is just a lab environment to try features and different implementation strategies, don't expect to get thrilling gameplay out of this project. We will link to the real deal once game is at a minimal playable state.

This project is not for profit and open source, it contains copyrighted images on a fair use basis.

## Using the randomizer in your own quest

To make use of the randomization feature in your quest, follow these steps:

1. Import the following files from this repository:
   - scripts/rando.lua
   - scripts/meta/chest.lua
   - scripts/meta/map.lua
2. Add the meta scripts to your *features.lua*:
    require("scripts/meta/chest")
    require("scripts/meta/map")
3. Edit the rando.lua file. Details below this list!
4. On every map that uses pickables (items plainly on the ground, not stored in a chest), add this line to the corresponding map script:
    map:update_pickables()
5. Import the rando.lua file in the script that initializes your game, most probably *initial_game.lua*. Make sure you make it a global variable, **don't use "local" on that line!**
    rando = require("scripts/rando")
6. Start the randomization in the same file of the previous step by providing the game object and the seed string you want to initialize the randomizer with:
    rando:generate(game, "seedstring")
7. save your changes and hit F5 ;)

## Customizing the randomizer

Depending on your game, you might want to customize way more than what will be shown here, but this is the bare minimum to get you started.

### Customizing the dungeon layout

This task consists of two parts: editing the *deungeon_definitions* table in rando.lua and implementing code in your map script to modify dynamic tiles.

Each entry in the *dungeon_definitions* table (one for each map that makes use of randomized variations) should have at least these three pieces of information:

- the *id* for that variant definition, stored in a savegame variable of the same name. Your map script will use this later on
- the *default_variant* to be used if there was no seed string provided on initializing the randomizer. I tend to use 1 here.
- the *max_variant* that your map will support. You might want to keep to low numbers, usually 2 - 4 is enough. Remember: you will have to script all those variants for your map and the players have to work through them.

Once you have randomized variant number set up like this, you can start coding your map script that uses it. You can move doors or bridges, move blocking elements around, just do some cosmetics or anything you like, really. Maybe implement a quick and easy variant of the dungeon and a tough one with more enemies or longer puzzles? Swap the tileset? Enable or disable spots that guarantee heart refills or magic potions? Move chests to different locations on the map? Be creative! Christopho has nice tutorials up on YouTube, but you would not be reading this document if you didn't know already, do you?

### Customizing loot drops

This section is split in two parts as well: going through all chests and pickables you want to be shuffled and add their items to either the *important_items* or *filler_items* tables and adding an entry in the *locations* table.

The randomizer will distribute the *important_items* first. Important items are those that enable the player to reach new locations they could not reach before. Think of bombs, bow, hookshot, flippers and so on.

After the important items have ben sent to their new locations, the randomizer will distribute the *filler_items*. Gems/Rupees, pieces of heart, ammunition, magic refills, tunics and such are considered filler items, they will not enhance the player abilities. At least not in a meaningful way for the randomizer.

Make sure to add each item to those lists, maybe even repeatedly. You don't want to hand out a single heart container, but several, right? And finding a second shield, sword, glove or bow will upgrade the old one. Also, there should be lots of money and ammunition to be found.

Now comes the tougher part: defining the logic for a particular location!

The table *locations* consists of one sub-table for each loot location to be randomized. And each location might have different requirements for the player to get there. For the randomizer to figure out which locations are available to the player given a certain loadout, you have to provide an expression to figure out what is needed.

In the most simple case the location is readily available, without any requirement at all other than to find the location. In that case, just enter "true" as the value for the *logic* field. Every *logic* expression should evaluate to boolean true or false in the end.

If the location requires an item, e.g. the hookshot, set the *logic* to "rando:has('hookshot')". *has* is a method on the randomizer to check if the item has been acquired already. This is also the reason we needed to add the file *rando.lua* as a global reference, not a local one in step 5 above: these expressions will be evaluated by the Lua function *loadstring()*, which can only access the global scope.

If the location depends on the variant of the dungeon layout or any other savegame variable, use this notation: "rando:sgvar('start_variant') == 1". 

Sooner than later you will need to combine several expressions to compute the availability of a location. Take this example expression:

    rando:sgvar('start_variant') == 1 or (rando:sgvar('start_variant') == 2 and rando:has('glove')) or (rando:sgvar('start_variant') == 3 and (rando:has('hookshot') or rando:has('flippers')))

It may look daunting at first, but it is really simple. Let's break that long expression down to its three main parts, any of which might evaluate to true:

    rando:sgvar('start_variant') == 1
    or (rando:sgvar('start_variant') == 2 and rando:has('glove'))
    or (rando:sgvar('start_variant') == 3 and (rando:has('hookshot') or rando:has('flippers')))

The first line is easy: the chest is available immediately on the first variant of the dungeon layout.

The second line sets this location as available if the dungeon layout uses variant 2 and the glove could be acquired beforehand.

The third line is for dungeon layout 3 and will additionally require either the hookshot or the flippers to reach.

Adding parentheses on groups of 2 or more conditions both makes a visual grouping difference and might be required for some logic constellations. You might want to read about operator precedence in Lua to fully understand when you really have to set parentheses and when you might get by without them.

If you are thinking about the logic of a particular location in a dungeon, keep in mind that the way to the dungeon might also include the use of some items or require certain savegame variables to be set. Maybe the chest in the dungeon only requires the glove level one, but to get to the dungeon you might need bombs and have helped an NPC, so check for those as well when considering the logic expression of any location in that dungeon.
